# MusalaDemo
This application was made as test project for [MusalaSoft](https://www.musala.com/) company.

#### Assignment
Find original assignment for this project [here](doc/ASSIGNMENT.md).

#### Author
- Name: Aleksandr Tsibenko
- Contacts: tsibenko.a.s@gmail.com, [LinkedIn profile](https://www.linkedin.com/in/aleksandr-tsibenko/)
- This repo link: https://gitlab.com/freeShlak/musalademo

#### Decisions
I had questions for some assignment requirements, so I've decided to describe some of my decisions [here](doc/DECISIONS.md).

#### Project Stack
1) Java 17;
2) Spring Boot;
3) Lombok;
4) Liquibase for db migrations and data loading;
5) H2 (keep everything in RAM for tests and in local file for other cases);
6) log4j2;
7) Gradle;
8) OpenAPI + Swagger for API documentation.

#### How to run application
- Using IDE (IDEA, for example): start `main` void at `MusalaDemoApplication` class.  
- Using gradle wrapper: run `./gradlew bootRun` from project root directory (installed java is required).  

#### How to run application tests
- Using IDE (IDEA, for example): use `Run 'Tests in 'musala-demo'` menu item at project root directory.
- Using gradle wrapper: run `./gradlew test` from project root directory (installed java is required).

#### Data storage
- ./data/demo.*.db - H2 database files.
- ./data/log/battery - statistic logs with drone battery capacity.
- ./data/medication/image - path for custom medication images (you could place your image here and write file name to medication table in image_url column).

All tables fills up with data automatically on application start.
If you want to change some default data - you could do it manually through `/h2-console` UI endpoint, 
or change records in source csv files - application will rewrite all data in your db after restart (only on csv changing).

#### Setup IDEA for development
1) Install [lombok plugin](https://plugins.jetbrains.com/plugin/6317-lombok).
2) Enable annotation processing (
   `Settings` ->
   `Build, Execution, Deployment` ->
   `Compiler` ->
   `Annotation Processors` ->
   `Enable annotation processing`
   );

#### API exploring
Use `/swagger-ui/index.html` UI endpoint for exploring application generated schema.