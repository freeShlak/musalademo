package atsibenko.musalademo.delivery.dao;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.repository.DeliveryRepository;
import atsibenko.musalademo.delivery.util.DeliveryMapper;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.repository.DroneRepository;
import atsibenko.musalademo.drone.util.DroneMapper;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.repository.MedicationRepository;
import atsibenko.musalademo.medication.util.MedicationMapper;
import atsibenko.musalademo.test.util.TestDataUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@DataJpaTest
@Import(DeliveryDaoImpl.class)
public class DeliveryDaoTest {
    @Autowired
    private DroneRepository droneRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private DeliveryRepository deliveryRepository;
    @Autowired
    private DeliveryDao dao;

    @Test
    public void given_FullInfoDelivery_when_GetShortInfo_then_MedicationItemsNotIncluded() {
        var delivery = TestDataUtil.buildDelivery();
        save(delivery);

        var result = dao.getShortInfo(delivery.getId());
        assertThat(result)
                .isPresent().get()
                .usingRecursiveComparison()
                .isEqualTo(delivery.toBuilder().items(null).build());
        assertThat(result.get().getItems()).isNull();
    }

    @Test
    public void given_FullInfoDelivery_when_GetFullInfo_then_MedicationItemsIncluded() {
        var delivery = TestDataUtil.buildDelivery();
        save(delivery);

        var result = dao.getFullInfo(delivery.getId());
        assertThat(result)
                .isPresent().get()
                .usingRecursiveComparison()
                .isEqualTo(delivery);
        assertThat(
                result.map(Delivery::getItems).stream()
                        .flatMap(Collection::stream)
                        .sorted(Comparator.comparing(Medication::getId))
                        .toList())
                .isNotNull()
                .isNotEmpty()
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(
                        Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .sorted(Comparator.comparing(Medication::getId))
                                .toList());
        assertThat(result.get().getDrone().getWeightLimit()).isEqualTo(delivery.getDrone().getWeightLimit());
    }

    @Test
    public void given_FullInfoDelivery_when_GetFullInfoWithWrongId_then_NothingFound() {
        save(TestDataUtil.buildDelivery());
        assertThat(dao.getFullInfo(UUID.randomUUID())).isEmpty();
    }

    @Test
    public void given_FullInfoDelivery_when_GetShortInfoWithWrongId_then_NothingFound() {
        save(TestDataUtil.buildDelivery());
        assertThat(dao.getShortInfo(UUID.randomUUID())).isEmpty();
    }

    @Test
    public void given_SavedMedicationAndDrone_when_CreateDelivery_then_CreatedCorrectly() {
        var expected = TestDataUtil.buildDelivery();
        save(expected.getDrone());
        Optional.ofNullable(expected.getItems()).ifPresent(this::save);

        var actual = dao.saveDelivery(expected);
        assertThat(actual)
                .usingRecursiveComparison()
                .isEqualTo(expected);
        assertThat(dao.getFullInfo(expected.getId()))
                .isPresent().get()
                .usingRecursiveComparison()
                .isEqualTo(expected);
    }

    @Test
    public void given_NotSavedMedicationAndDrone_when_CreateDelivery_then_ThrowException() {
        var delivery = TestDataUtil.buildDelivery();

        assertThatThrownBy(() -> dao.saveDelivery(delivery));
        assertThat(
                medicationRepository.findAllById(
                        Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .map(Medication::getId)
                                .toList()))
                .isEmpty();
        assertThat(droneRepository.findById(delivery.getId())).isEmpty();
    }

    private void save(Delivery delivery) {
        save(delivery.getDrone());
        Optional.ofNullable(delivery.getItems()).ifPresent(this::save);
        deliveryRepository.save(DeliveryMapper.map(delivery));
    }

    private void save(Drone drone) {
        droneRepository.save(DroneMapper.map(drone));
    }

    private void save(List<Medication> items) {
        medicationRepository.saveAll(items.stream().map(MedicationMapper::map).toList());
    }
}
