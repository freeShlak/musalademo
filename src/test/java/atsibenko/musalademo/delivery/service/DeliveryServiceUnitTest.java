package atsibenko.musalademo.delivery.service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.delivery.dao.DeliveryDao;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.drone.dao.DroneDao;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.medication.dao.MedicationDao;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.test.util.TestDataUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DeliveryServiceUnitTest {
    @Mock
    private DeliveryDao deliveryDao;
    @Mock
    private MedicationDao medicationDao;
    @Mock
    private DroneDao droneDao;

    @InjectMocks
    private DeliveryService service;

    @Test
    public void given_WrongDroneId_when_CreateDelivery_then_Throw() {
        var droneId = UUID.randomUUID();
        var medicationIds = List.of(UUID.randomUUID());
        when(droneDao.findById(droneId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.createDelivery(
                droneId,
                medicationIds
        )).hasMessageContaining("Can't find drone").hasMessageContaining(droneId.toString());
    }

    @Test
    public void given_UnavailableDrone_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone(DroneStatus.LOADING);
        var medicationIds = List.of(UUID.randomUUID());

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                medicationIds
        )).hasMessageContaining("unavailable").hasMessageContaining(drone.getId().toString());
    }

    @Test
    public void given_LowDroneBatteryLevel_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        drone.setBatteryCapacity(10);

        var medicationIds = List.of(UUID.randomUUID());

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                medicationIds
        )).hasMessageContaining("10%");
    }

    @Test
    public void given_IncorrectMedication_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        var medicationIds = List.of(UUID.randomUUID());

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        when(medicationDao.findByIds(medicationIds)).thenReturn(List.of());
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                medicationIds
        )).hasMessageContaining("Can't find").hasMessageContaining(medicationIds.get(0).toString());
    }

    @Test
    public void given_IncorrectOneMedication_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        var existingMedication = TestDataUtil.buildMedication();
        var medicationIds = List.of(existingMedication.getId(), UUID.randomUUID());

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        when(medicationDao.findByIds(medicationIds)).thenReturn(List.of(existingMedication));
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                medicationIds
        )).hasMessageContaining("Can't find").hasMessageContaining(medicationIds.get(1).toString());
    }

    @Test
    public void given_MedicationUnavailable_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        var medication = TestDataUtil.buildMedication(MedicationStatus.RESERVED);

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        when(medicationDao.findByIds(any())).thenReturn(List.of(medication));
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                List.of(medication.getId())
        )).hasMessageContaining("unavailable").hasMessageContaining(medication.getId().toString());
    }

    @Test
    public void given_OverweightWithOneItem_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        var medication = TestDataUtil.buildMedication();
        medication.setWeight(1000);

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        when(medicationDao.findByIds(any())).thenReturn(List.of(medication));
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                List.of(medication.getId())
        )).hasMessageContaining("Overweight");
    }

    @Test
    public void given_OverweightWithSeveralItems_when_CreateDelivery_then_Throw() {
        var drone = TestDataUtil.buildDrone();
        var medications = List.of(
                TestDataUtil.buildMedication(),
                TestDataUtil.buildMedication(),
                TestDataUtil.buildMedication(),
                TestDataUtil.buildMedication(),
                TestDataUtil.buildMedication()
        );
        medications.forEach(item -> item.setWeight(200));

        when(droneDao.findById(drone.getId())).thenReturn(Optional.of(drone));
        when(medicationDao.findByIds(any())).thenReturn(medications);
        assertThatThrownBy(() -> service.createDelivery(
                drone.getId(),
                medications.stream().map(Medication::getId).toList()
        )).hasMessageContaining("Overweight");
    }

    @Test
    public void given_CorrectRequest_when_CreateDelivery_then_CreatedAndDeliveryReturned() {
        var expectedDelivery = TestDataUtil.buildDelivery();
        var rawDrone = expectedDelivery.getDrone().toBuilder().status(DroneStatus.IDLE).build();
        var rawMedications = Optional.ofNullable(expectedDelivery.getItems()).stream()
                .flatMap(Collection::stream)
                .map(item -> item.toBuilder().status(MedicationStatus.AVAILABLE).build())
                .toList();

        when(droneDao.findById(any())).thenReturn(Optional.of(rawDrone));
        when(droneDao.save(any())).thenReturn(expectedDelivery.getDrone());

        when(medicationDao.findByIds(any())).thenReturn(rawMedications);
        when(medicationDao.saveAll(any())).thenReturn(expectedDelivery.getItems());

        when(deliveryDao.saveDelivery(any())).thenReturn(expectedDelivery);

        var response = service.createDelivery(
                rawDrone.getId(),
                rawMedications.stream().map(Medication::getId).toList()
        );

        assertThat(response)
                .usingRecursiveComparison()
                .isEqualTo(expectedDelivery);
    }

    @Test
    public void given_WrongDeliveryId_when_RegisterDelivery_then_Throw() {
        var deliveryId = UUID.randomUUID();
        when(deliveryDao.getFullInfo(any())).thenReturn(Optional.empty());
        assertThatThrownBy(() -> service.registerDelivery(
                deliveryId
        )).hasMessageContaining("Can't find delivery").hasMessageContaining(deliveryId.toString());
    }

    @Test
    public void given_DeliveryAlreadyDelivered_when_RegisterDelivery_then_Throw() {
        var delivery = TestDataUtil.buildDelivery(
                DroneStatus.DELIVERED,
                MedicationStatus.UNAVAILABLE,
                DeliveryStatus.DELIVERED
        );
        when(deliveryDao.getFullInfo(any())).thenReturn(Optional.of(delivery));
        assertThatThrownBy(() -> service.registerDelivery(
                delivery.getId()
        )).hasMessageContaining("delivery in status").hasMessageContaining(delivery.getStatus().toString());
    }

    @Test
    public void given_DeliveryInCorrectState_when_RegisterDelivery_then_RegisteredAndReturned() {
        var deliveryInProgress = TestDataUtil.buildDelivery(
                DroneStatus.DELIVERING,
                MedicationStatus.RESERVED,
                DeliveryStatus.DELIVERING
        );
        var deliveryRegistered = deliveryInProgress.toBuilder()
                .status(DeliveryStatus.DELIVERED)
                .drone(deliveryInProgress.getDrone().toBuilder()
                        .status(DroneStatus.DELIVERED)
                        .build())
                .items(Optional.ofNullable(deliveryInProgress.getItems()).stream()
                        .flatMap(Collection::stream)
                        .map(item -> item.toBuilder().status(MedicationStatus.UNAVAILABLE).build())
                        .toList())
                .build();

        when(deliveryDao.getFullInfo(any())).thenReturn(Optional.of(deliveryInProgress));
        when(droneDao.save(any())).thenReturn(deliveryRegistered.getDrone());
        when(medicationDao.saveAll(any())).thenReturn(deliveryRegistered.getItems());
        when(deliveryDao.saveDelivery(any())).thenReturn(deliveryRegistered);

        var response = service.registerDelivery(deliveryInProgress.getId());
        assertThat(response)
                .usingRecursiveComparison()
                .isEqualTo(deliveryRegistered);
    }
}
