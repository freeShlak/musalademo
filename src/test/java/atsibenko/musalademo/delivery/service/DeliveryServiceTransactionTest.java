package atsibenko.musalademo.delivery.service;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.delivery.repository.DeliveryRepository;
import atsibenko.musalademo.delivery.util.DeliveryMapper;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.drone.repository.DroneEntity;
import atsibenko.musalademo.drone.repository.DroneEntityStatus;
import atsibenko.musalademo.drone.repository.DroneRepository;
import atsibenko.musalademo.drone.util.DroneMapper;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.repository.MedicationEntityStatus;
import atsibenko.musalademo.medication.repository.MedicationRepository;
import atsibenko.musalademo.medication.util.MedicationMapper;
import atsibenko.musalademo.test.util.TestDataUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@AutoConfigureTestDatabase
public class DeliveryServiceTransactionTest {
    @Autowired
    private DroneRepository droneRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @SpyBean
    private DeliveryRepository deliveryRepository;
    @Autowired
    private DeliveryService service;

    /**
     * Clearing involved tables because of transactions testing.
     */
    @AfterEach
    public void clearDB() {
        deliveryRepository.deleteAll();
        medicationRepository.deleteAll();
        droneRepository.deleteAll();
    }

    @Test
    public void given_ExceptionOnDeliveryCreation_when_CreateDelivery_then_NotChangedStatuses() {
        var delivery = TestDataUtil.buildDelivery();
        var droneId = delivery.getDrone().getId();
        var medicationIds = Optional.ofNullable(delivery.getItems()).stream()
                .flatMap(Collection::stream)
                .map(Medication::getId).toList();

        save(delivery.getDrone().toBuilder().status(DroneStatus.IDLE).build());
        save(Optional.ofNullable(delivery.getItems()).stream()
                .flatMap(Collection::stream)
                .map(item -> item.toBuilder().status(MedicationStatus.AVAILABLE).build())
                .toList());


        Mockito.doThrow(RuntimeException.class).when(deliveryRepository).save(any());
        assertThatThrownBy(() -> service.createDelivery(
                droneId,
                medicationIds
        ));

        var droneStatus = droneRepository.findById(delivery.getDrone().getId())
                .map(DroneEntity::getStatus);
        assertThat(droneStatus)
                .isPresent().get()
                .isEqualTo(DroneEntityStatus.IDLE);

        var foundMedications = medicationRepository.findAllById(medicationIds);
        assertThat(foundMedications).hasSameSizeAs(medicationIds);
        foundMedications.forEach(item -> assertThat(item.getStatus()).isEqualTo(MedicationEntityStatus.AVAILABLE));
    }

    @Test
    public void given_CorrectParams_when_CreateDelivery_then_CorrectDBInfo() {
        var delivery = TestDataUtil.buildDelivery();
        var droneId = delivery.getDrone().getId();
        var medicationIds = Optional.ofNullable(delivery.getItems()).stream()
                .flatMap(Collection::stream)
                .map(Medication::getId).toList();

        save(delivery.getDrone().toBuilder().status(DroneStatus.IDLE).build());
        save(Optional.ofNullable(delivery.getItems()).stream()
                .flatMap(Collection::stream)
                .map(item -> item.toBuilder().status(MedicationStatus.AVAILABLE).build())
                .toList());

        var result = service.createDelivery(
                droneId,
                medicationIds
        );
        delivery.setId(result.getId());

        assertThat(withSortedItems(result))
                .usingRecursiveComparison()
                .isEqualTo(withSortedItems(delivery));
    }

    @Test
    public void given_ExceptionOnDeliverySaving_when_RegisterDelivery_then_NotChangedStatuses() {
        var delivery = TestDataUtil.buildDelivery(
                DroneStatus.DELIVERING,
                MedicationStatus.RESERVED,
                DeliveryStatus.DELIVERING
        );

        save(delivery.getDrone());
        save(Optional.ofNullable(delivery.getItems()).orElseThrow());
        save(delivery);

        Mockito.doThrow(RuntimeException.class).when(deliveryRepository).save(any());
        assertThatThrownBy(() -> service.registerDelivery(delivery.getId()));

        var droneStatus = droneRepository.findById(delivery.getDrone().getId())
                .map(DroneEntity::getStatus);
        assertThat(droneStatus)
                .isPresent().get()
                .isEqualTo(DroneEntityStatus.DELIVERING);

        var foundMedications = medicationRepository.findAllById(
                Optional.ofNullable(delivery.getItems()).stream()
                        .flatMap(Collection::stream)
                        .map(Medication::getId).toList()
        );
        assertThat(foundMedications).hasSameSizeAs(delivery.getItems());
        foundMedications.forEach(item -> assertThat(item.getStatus()).isEqualTo(MedicationEntityStatus.RESERVED));
    }

    @Test
    public void given_CorrectParams_when_RegisterDelivery_then_CorrectDBInfo() {
        var deliveryInProgress = TestDataUtil.buildDelivery(
                DroneStatus.DELIVERING,
                MedicationStatus.RESERVED,
                DeliveryStatus.DELIVERING
        );
        var deliveryRegistered = deliveryInProgress.toBuilder()
                .status(DeliveryStatus.DELIVERED)
                .drone(deliveryInProgress.getDrone().toBuilder()
                        .status(DroneStatus.DELIVERED)
                        .build())
                .items(Optional.ofNullable(deliveryInProgress.getItems()).stream()
                        .flatMap(Collection::stream)
                        .map(item -> item.toBuilder().status(MedicationStatus.UNAVAILABLE).build())
                        .toList())
                .build();

        save(deliveryInProgress.getDrone());
        save(Optional.ofNullable(deliveryInProgress.getItems()).orElseThrow());
        save(deliveryInProgress);

        var result = service.registerDelivery(deliveryRegistered.getId());

        assertThat(withSortedItems(result))
                .usingRecursiveComparison()
                .isEqualTo(withSortedItems(deliveryRegistered));
    }

    private void save(Delivery delivery) {
        deliveryRepository.save(DeliveryMapper.map(delivery));
    }

    private void save(Drone drone) {
        droneRepository.save(DroneMapper.map(drone));
    }

    private void save(List<Medication> items) {
        medicationRepository.saveAll(items.stream().map(MedicationMapper::map).toList());
    }

    private Delivery withSortedItems(Delivery delivery) {
        return delivery.toBuilder()
                .items(
                        Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .sorted(Comparator.comparing(Medication::getId))
                                .toList())
                .build();
    }
}
