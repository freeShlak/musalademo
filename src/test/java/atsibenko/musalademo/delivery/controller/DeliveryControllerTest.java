package atsibenko.musalademo.delivery.controller;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.configuration.ObjectMapperConfiguration;
import atsibenko.musalademo.delivery.dto.CreateDeliveryRequest;
import atsibenko.musalademo.delivery.dto.DeliveryDto;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.delivery.service.DeliveryService;
import atsibenko.musalademo.delivery.util.DeliveryMapper;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.exception.ErrorResponse;
import atsibenko.musalademo.medication.dto.MedicationDto;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.util.MedicationMapper;
import atsibenko.musalademo.test.util.TestDataUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DeliveryController.class)
@Import(ObjectMapperConfiguration.class)
public class DeliveryControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private DeliveryService deliveryService;

    @Test
    public void given_FullInfoDelivery_when_GetFullInfo_then_ParsedWithItemsAndCorrectly() throws Exception {
        var delivery = TestDataUtil.buildDelivery();

        when(deliveryService.getFullInfo(any())).thenReturn(Optional.of(delivery));
        var result = mvc.perform(get("/delivery/{deliveryId}/full-info", delivery.getId()))
                .andExpect(status().isOk())
                .andReturn();

        var content = result.getResponse().getContentAsString();
        assertThat(content).contains("items");
        var parsedResult = om.readValue(content, DeliveryDto.class);

        assertThat(parsedResult)
                .usingRecursiveComparison()
                .isEqualTo(DeliveryMapper.toDto(delivery));

        assertThat(parsedResult.getItems())
                .isNotNull()
                .isNotEmpty();

        assertThat(parsedResult.getItems().stream().sorted(Comparator.comparing(MedicationDto::getId)).toList())
                .isNotEmpty()
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(
                        Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .map(MedicationMapper::toDto)
                                .sorted(Comparator.comparing(MedicationDto::getId))
                                .toList());
    }

    @Test
    public void given_ShortInfoDelivery_when_GetShortInfo_then_ParsedWithoutItemsAndCorrectly() throws Exception {
        var delivery = TestDataUtil.buildDelivery().toBuilder().items(null).build();

        when(deliveryService.getShortInfo(any())).thenReturn(Optional.of(delivery));
        var result = mvc.perform(get("/delivery/{deliveryId}/short-info", delivery.getId()))
                .andExpect(status().isOk())
                .andReturn();

        var content = result.getResponse().getContentAsString();
        assertThat(content).doesNotContain("items");
        var parsedResult = om.readValue(content, DeliveryDto.class);

        assertThat(parsedResult)
                .usingRecursiveComparison()
                .isEqualTo(DeliveryMapper.toDto(delivery));

        assertThat(parsedResult.getItems()).isNull();
    }

    @Test
    public void given_NotCorrectId_when_GetFullInfo_then_NotFound() throws Exception {
        var id = UUID.randomUUID();
        when(deliveryService.getFullInfo(any())).thenReturn(Optional.empty());
        var result = mvc.perform(get("/delivery/{deliveryId}/full-info", id))
                .andExpect(status().isNotFound())
                .andReturn();

        var errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).contains(id.toString());
    }

    @Test
    public void given_NotCorrectId_when_GetShortInfo_then_NotFound() throws Exception {
        var id = UUID.randomUUID();
        when(deliveryService.getShortInfo(any())).thenReturn(Optional.empty());
        var result = mvc.perform(get("/delivery/{deliveryId}/short-info", id))
                .andExpect(status().isNotFound())
                .andReturn();

        var errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).contains(id.toString());
    }

    @Test
    public void given_CorrectCreateRequest_when_Create_then_FullyAndCorrectlyParsed() throws Exception {
        var delivery = TestDataUtil.buildDelivery();
        var request = om.writeValueAsString(
                CreateDeliveryRequest.builder()
                        .droneId(delivery.getDrone().getId())
                        .medicationItems(Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .map(Medication::getId)
                                .toList())
                        .build()
        );

        when(deliveryService.createDelivery(any(), any())).thenReturn(delivery);
        var result = mvc.perform(post("/delivery/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isOk())
                .andReturn();

        var content = result.getResponse().getContentAsString();
        assertThat(content).contains("items");
        var parsedResult = om.readValue(content, DeliveryDto.class);

        assertThat(parsedResult)
                .usingRecursiveComparison()
                .isEqualTo(DeliveryMapper.toDto(delivery));

        assertThat(parsedResult.getItems())
                .isNotNull()
                .isNotEmpty();

        assertThat(parsedResult.getItems().stream().sorted(Comparator.comparing(MedicationDto::getId)).toList())
                .isNotEmpty()
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(
                        Optional.ofNullable(delivery.getItems()).stream()
                                .flatMap(Collection::stream)
                                .map(MedicationMapper::toDto)
                                .sorted(Comparator.comparing(MedicationDto::getId))
                                .toList());
    }

    @Test
    public void given_InnerValidationException_when_CreateDelivery_then_BadRequest() throws Exception {
        var request = om.writeValueAsString(
                CreateDeliveryRequest.builder()
                        .droneId(UUID.randomUUID())
                        .medicationItems(List.of(UUID.randomUUID()))
                        .build()
        );

        when(deliveryService.createDelivery(any(), any())).thenThrow(new IllegalArgumentException("test"));
        var result = mvc.perform(post("/delivery/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();

        var error = om.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(error.getTimestamp()).isNotNull();
        assertThat(error.getErrors()).isEqualTo(List.of("test"));
        assertThat(error.getStatus()).isEqualTo(400);
    }

    @Test
    public void given_EmptyRequest_when_CreateDelivery_then_BadRequest() throws Exception {
        var request = om.writeValueAsString(CreateDeliveryRequest.builder().build());

        var result = mvc.perform(post("/delivery/create")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(status().isBadRequest())
                .andReturn();

        var error = om.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(error.getTimestamp()).isNotNull();
        assertThat(error.getErrors()).hasSize(2);
        assertThat(error.getStatus()).isEqualTo(400);
    }

    @Test
    public void given_SuccessfulRegistration_when_RegisterDelivery_then_FullyAndCorrectlyParsed() throws Exception {
        var delivery = TestDataUtil.buildDelivery(
                DroneStatus.DELIVERED,
                MedicationStatus.UNAVAILABLE,
                DeliveryStatus.DELIVERED
        );

        when(deliveryService.registerDelivery(any())).thenReturn(delivery);
        var result = mvc.perform(post("/delivery/{deliveryId}/register", delivery.getId()))
                .andExpect(status().isOk())
                .andReturn();

        var parsedResult = om.readValue(result.getResponse().getContentAsString(), DeliveryDto.class);

        assertThat(parsedResult)
                .usingRecursiveComparison()
                .isEqualTo(DeliveryMapper.toDto(delivery));
    }

    @Test
    public void given_InnerValidationException_when_RegisterDelivery_then_BadRequest() throws Exception {
        when(deliveryService.registerDelivery(any())).thenThrow(new IllegalArgumentException("test"));
        var result = mvc.perform(post("/delivery/{deliveryId}/register", UUID.randomUUID()))
                .andExpect(status().isBadRequest())
                .andReturn();

        var error = om.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);
        assertThat(error.getTimestamp()).isNotNull();
        assertThat(error.getErrors()).isEqualTo(List.of("test"));
        assertThat(error.getStatus()).isEqualTo(400);
    }
}
