package atsibenko.musalademo.medication.controller;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import atsibenko.musalademo.configuration.ObjectMapperConfiguration;
import atsibenko.musalademo.medication.dao.MedicationDao;
import atsibenko.musalademo.medication.dto.MedicationDto;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.util.MedicationMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MedicationController.class)
@Import(ObjectMapperConfiguration.class)
public class MedicationControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private MedicationDao medicationDao;

    private final Random random = new Random();

    @Test
    public void given_TwoAvailableMedications_when_GetAvailable_then_ParsedCorrectly() throws Exception {
        var objectResponse = List.of(
                randomAvailableMedication(),
                randomAvailableMedication()
        );

        when(medicationDao.findAllByStatus(anyCollection(), any())).thenAnswer(i -> {
            var args = i.getArguments();
            var pageable = (Pageable) args[1];
            return new PageImpl<>(
                    objectResponse,
                    pageable,
                    pageable.getPageSize() + 1000
            );
        });
        var result = mvc.perform(get("/medication")
                        .queryParam("size", "2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.size", is(2)))
                .andExpect(jsonPath("$.number", is(0)))
                .andExpect(jsonPath("$.totalElements", is(1002)))
                .andReturn();

        var response = result.getResponse().getContentAsString();
        var contentNode = om.readTree(response).findValue("content");

        var parsedResult = om.readValue(
                om.writeValueAsString(contentNode),
                new TypeReference<List<MedicationDto>>() {
                }
        );
        assertThat(parsedResult).containsExactlyInAnyOrderElementsOf(
                objectResponse.stream()
                        .map(medication -> MedicationMapper.toDto(medication)
                                .toBuilder()
                                .status(MedicationMapper.toDto(medication.getStatus()))
                                .build())
                        .toList());
    }

    private Medication randomAvailableMedication() {
        return Medication.builder()
                .id(UUID.randomUUID())
                .name("name_" + random.nextInt(1000))
                .code("code_" + random.nextInt(1000))
                .weight(random.nextInt(500))
                .status(MedicationStatus.values()[random.nextInt(MedicationStatus.values().length)])
                .imgUrl("/path/to/img/" + random.nextInt(1000) + ".jpg")
                .build();
    }
}
