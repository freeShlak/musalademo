package atsibenko.musalademo.medication.dao;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.repository.MedicationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(MedicationDaoImpl.class)
public class MedicationDaoTest {
    @Autowired
    private MedicationRepository repository;
    @Autowired
    private MedicationDao dao;

    @Test
    public void given_TwoMedicationItems_when_findByIds_then_FoundAndEqual() {
        var items = saveAvailableAndUnavailableEntities();

        var searchingIds = items.stream()
                .map(Medication::getId)
                .toList();

        var actual = dao.findByIds(searchingIds);
        assertThat(actual).containsExactlyInAnyOrderElementsOf(items);
    }

    @Test
    public void given_TwoMedicationItems_when_findByOnlyOneId_then_FoundAndEqual() {
        var items = saveAvailableAndUnavailableEntities();
        var expected = items.getFirst();
        var actual = dao.findByIds(List.of(expected.getId()));
        assertThat(actual).containsOnly(expected);
    }

    @Test
    public void given_TwoMedicationItems_when_findByOtherIds_then_NothingFound() {
        saveAvailableAndUnavailableEntities();
        assertThat(dao.findByIds(List.of(UUID.randomUUID(), UUID.randomUUID()))).isEmpty();
    }

    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByAllStatuses_then_FoundAndEqual() {
        var items = saveAvailableAndUnavailableEntities();

        var searchingStatuses = Arrays.asList(MedicationStatus.values());

        var actual = dao.findAllByStatus(searchingStatuses, Pageable.unpaged()).stream().toList();
        assertThat(actual).containsExactlyInAnyOrderElementsOf(items);
    }

    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByDifferentStatus_then_NothingFound() {
        saveAvailableAndUnavailableEntities();
        var searchingStatuses = List.of(MedicationStatus.RESERVED);

        var actual = dao.findAllByStatus(searchingStatuses, Pageable.unpaged()).stream().toList();
        assertThat(actual).isEmpty();
    }

    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByOnlyOneStatus_then_OnlyOneFoundAndEqual() {
        var items = saveAvailableAndUnavailableEntities();
        var searchingStatuses = List.of(MedicationStatus.AVAILABLE);

        var expected = items.getFirst();
        var actual = dao.findAllByStatus(searchingStatuses, Pageable.unpaged());
        assertThat(actual).containsOnly(expected);
    }

    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByAllStatusesAndOnePageSize_then_FoundOnlyOne() {
        var items = saveAvailableAndUnavailableEntities();

        var searchingStatuses = Arrays.asList(MedicationStatus.values());

        var expected = items.stream()
                .min(Comparator.comparing(Medication::getName))
                .orElseThrow();

        var pageRequest = PageRequest.of(0, 1, Sort.by(Sort.Direction.ASC, "name"));
        var actual = dao.findAllByStatus(searchingStatuses, pageRequest);

        assertThat(actual).containsOnly(expected);
        assertThat(actual.getNumberOfElements()).isEqualTo(1);
        assertThat(actual.getTotalElements()).isEqualTo(2);
        assertThat(actual.getTotalPages()).isEqualTo(2);
        assertThat(actual.isFirst()).isTrue();
        assertThat(actual.isLast()).isFalse();
    }

    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByIdsAndOnlyOneStatus_then_OnlyOneFoundAndEqual() {
        var items = saveAvailableAndUnavailableEntities();
        var searchingStatuses = List.of(MedicationStatus.AVAILABLE);
        var searchingIds = items.stream().map(Medication::getId).toList();

        var expected = items.getFirst();
        var actual = dao.findAllByIdsAndStatus(searchingIds, searchingStatuses, Pageable.unpaged());
        assertThat(actual).containsOnly(expected);
    }

    @Test
    public void given_TwoMedicationItemsInTheSameStatus_when_findByOnlyOneIdAndStatus_then_OnlyOneFoundAndEqual() {
        var items = saveAvailableEntities();

        var expected = items.getLast();

        var searchingStatuses = List.of(expected.getStatus());
        var searchingIds = List.of(expected.getId());

        var actual = dao.findAllByIdsAndStatus(searchingIds, searchingStatuses, Pageable.unpaged());
        assertThat(actual).containsOnly(expected);
    }


    @Test
    public void given_TwoMedicationItemsInDifferentStatuses_when_findByIdsAndStatusesAndOnePageSize_then_FoundOnlyOne() {
        var items = saveAvailableAndUnavailableEntities();

        var searchingStatuses = Arrays.asList(MedicationStatus.values());
        var searchingIds = items.stream().map(Medication::getId).toList();

        var expected = items.stream()
                .min(Comparator.comparing(Medication::getName))
                .orElseThrow();

        var pageRequest = PageRequest.of(0, 1, Sort.by(Sort.Direction.ASC, "name"));
        var actual = dao.findAllByIdsAndStatus(searchingIds, searchingStatuses, pageRequest);

        assertThat(actual).containsOnly(expected);
        assertThat(actual.getNumberOfElements()).isEqualTo(1);
        assertThat(actual.getTotalElements()).isEqualTo(2);
        assertThat(actual.getTotalPages()).isEqualTo(2);
        assertThat(actual.isFirst()).isTrue();
        assertThat(actual.isLast()).isFalse();
    }

    private List<Medication> saveAvailableAndUnavailableEntities() {
        var entities = List.of(
                Medication.builder()
                        .id(UUID.randomUUID())
                        .name("some_name_1")
                        .weight(251)
                        .code("SOME_CODE_1")
                        .status(MedicationStatus.AVAILABLE)
                        .imgUrl("/some/db/path/img_1.png")
                        .build(),
                Medication.builder()
                        .id(UUID.randomUUID())
                        .name("some_name_2")
                        .weight(252)
                        .code("SOME_CODE_2")
                        .status(MedicationStatus.UNAVAILABLE)
                        .imgUrl("/some/db/path/img_2.png")
                        .build()
        );

        dao.saveAll(entities);

        return entities;
    }

    private List<Medication> saveAvailableEntities() {
        var entities = List.of(
                Medication.builder()
                        .id(UUID.randomUUID())
                        .name("some_name_1")
                        .weight(251)
                        .code("SOME_CODE_1")
                        .status(MedicationStatus.AVAILABLE)
                        .imgUrl("/some/db/path/img_1.png")
                        .build(),
                Medication.builder()
                        .id(UUID.randomUUID())
                        .name("some_name_2")
                        .weight(252)
                        .code("SOME_CODE_2")
                        .status(MedicationStatus.AVAILABLE)
                        .imgUrl("/some/db/path/img_2.png")
                        .build()
        );

        dao.saveAll(entities);

        return entities;
    }
}
