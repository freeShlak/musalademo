package atsibenko.musalademo.test.util;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.model.DroneModel;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestDataUtil {

    public Delivery buildDelivery() {
        return buildDelivery(DroneStatus.LOADING, MedicationStatus.RESERVED, DeliveryStatus.LOADING);
    }

    public Delivery buildDelivery(
            DroneStatus droneStatus,
            MedicationStatus itemsStatus,
            DeliveryStatus deliveryStatus
    ) {
        var drone = buildDrone(droneStatus);
        var items = List.of(
                TestDataUtil.buildMedication(itemsStatus),
                TestDataUtil.buildMedication(itemsStatus)
        );
        return Delivery.builder()
                .id(UUID.randomUUID())
                .drone(drone)
                .items(items)
                .capacity(items.stream().mapToInt(Medication::getWeight).sum())
                .status(deliveryStatus)
                .build();
    }

    public Drone buildDrone() {
        return buildDrone(DroneStatus.IDLE);
    }

    public Drone buildDrone(DroneStatus status) {
        return Drone.builder()
                .id(UUID.randomUUID())
                .weightLimit(500)
                .batteryCapacity(50)
                .serialNumber("DRONE_SE")
                .model(DroneModel.HEAVYWEIGHT)
                .status(status)
                .build();
    }

    public Medication buildMedication() {
        return buildMedication(MedicationStatus.AVAILABLE);
    }

    public Medication buildMedication(MedicationStatus status) {
        return Medication.builder()
                .id(UUID.randomUUID())
                .weight(250)
                .code("CODE")
                .name("NAME")
                .status(status)
                .imgUrl("/path/placeholder.png")
                .build();
    }
}
