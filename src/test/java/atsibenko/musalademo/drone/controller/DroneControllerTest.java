package atsibenko.musalademo.drone.controller;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import atsibenko.musalademo.configuration.ObjectMapperConfiguration;
import atsibenko.musalademo.drone.dao.DroneDao;
import atsibenko.musalademo.drone.dto.DroneDto;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.model.DroneModel;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.drone.util.DroneMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DroneController.class)
@Import(ObjectMapperConfiguration.class)
public class DroneControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private DroneDao droneDao;

    private final Random random = new Random();

    @Test
    public void given_TwoAvailableDrones_when_GetAvailable_then_ParsedCorrectly() throws Exception {
        var objectResponse = List.of(
                randomAvailableDrone(),
                randomAvailableDrone()
        );

        when(droneDao.getAvailableDrones()).thenReturn(objectResponse);
        var result = mvc.perform(get("/drone/available"))
                .andExpect(status().isOk())
                .andReturn();

        var content = result.getResponse().getContentAsString();
        var parsedResult = om.readValue(
                content,
                new TypeReference<List<DroneDto>>() {
                }
        );
        assertThat(parsedResult)
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(objectResponse.stream().map(DroneMapper::toDto).toList());
    }

    @Test
    public void given_CorrectDroneId_when_GetState_then_Found() throws Exception {
        var objectResponse = randomAvailableDrone();

        when(droneDao.findById(any())).thenReturn(Optional.of(objectResponse));
        var result = mvc.perform(get("/drone/{droneId}/state", objectResponse.getId()))
                .andExpect(status().isOk())
                .andReturn();

        var content = result.getResponse().getContentAsString();
        var parsedResult = om.readValue(content, DroneDto.class);
        assertThat(parsedResult)
                .usingRecursiveComparison()
                .isEqualTo(DroneMapper.toDto(objectResponse));
    }

    @Test
    public void given_IncorrectDroneId_when_GetState_then_NotFound() throws Exception {
        var id = UUID.randomUUID();

        when(droneDao.findById(any())).thenReturn(Optional.empty());
        var result = mvc.perform(get("/drone/{droneId}/state", id))
                .andExpect(status().isNotFound())
                .andReturn();

        var errorMessage = result.getResponse().getErrorMessage();
        assertThat(errorMessage).contains(id.toString());
    }

    private Drone randomAvailableDrone() {
        return Drone.builder()
                .id(UUID.randomUUID())
                .serialNumber("test_serial_number_" + random.nextInt(1000))
                .model(DroneModel.values()[random.nextInt(DroneModel.values().length)])
                .weightLimit(random.nextInt(500))
                .batteryCapacity(random.nextInt(26, 100))
                .status(DroneStatus.IDLE)
                .build();
    }
}
