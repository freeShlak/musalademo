package atsibenko.musalademo.drone.dao;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.drone.repository.DroneEntity;
import atsibenko.musalademo.drone.repository.DroneEntityModel;
import atsibenko.musalademo.drone.repository.DroneEntityStatus;
import atsibenko.musalademo.drone.repository.DroneRepository;
import atsibenko.musalademo.drone.util.DroneValidator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@Import(DroneDaoImpl.class)
public class DroneDaoTest {
    @Autowired
    private DroneRepository repository;
    @Autowired
    private DroneDao dao;

    @Test
    public void given_DroneInIdleStatus_when_getAvailableDrones_then_FoundAndEqual() {
        var entity = buildDroneEntity();

        repository.save(entity);
        assertThat(dao.getAvailableDrones())
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(List.of(entity));
    }

    @Test
    public void given_DroneInInappropriateStatus_when_getAvailableDrones_then_NothingFound() {
        var entity = buildDroneEntity(DroneEntityStatus.DELIVERING);

        repository.save(entity);
        assertThat(dao.getAvailableDrones()).isEmpty();
    }

    @Test
    public void given_DroneWithLowBatteryLevel_when_getAvailableDrones_then_NothingFound() {
        var entity = buildDroneEntity(10);

        repository.save(entity);
        assertThat(dao.getAvailableDrones()).isEmpty();
    }

    @Test
    public void given_DroneWithMinimalBatteryLevel_when_getAvailableDrones_then_FoundAndEqual() {
        var entity = buildDroneEntity(DroneValidator.MINIMAL_BATTERY_CHARGE_PERCENT);

        repository.save(entity);
        assertThat(dao.getAvailableDrones())
                .usingRecursiveFieldByFieldElementComparator()
                .isEqualTo(List.of(entity));
    }

    @Test
    public void given_AllFieldsDrone_when_findById_then_FoundAndEqual() {
        var entity = buildDroneEntity();

        repository.save(entity);
        assertThat(dao.findById(entity.getId()))
                .isPresent().get()
                .usingRecursiveComparison()
                .isEqualTo(entity);
    }

    @Test
    public void given_AllFieldsDrone_when_findByAnotherId_then_NothingFound() {
        var entity = buildDroneEntity();

        repository.save(entity);
        assertThat(dao.findById(UUID.randomUUID())).isEmpty();
    }

    private static DroneEntity buildDroneEntity() {
        return buildDroneEntity(DroneEntityStatus.IDLE);
    }

    private static DroneEntity buildDroneEntity(DroneEntityStatus status) {
        return buildDroneEntity(status, 65);
    }

    private static DroneEntity buildDroneEntity(int batteryLevel) {
        return buildDroneEntity(DroneEntityStatus.IDLE, batteryLevel);
    }

    private static DroneEntity buildDroneEntity(DroneEntityStatus status, int batteryLevel) {
        return DroneEntity.builder()
                .id(UUID.randomUUID())
                .serialNumber("some_drone_sn")
                .model(DroneEntityModel.HEAVYWEIGHT)
                .weightLimit(500)
                .batteryCapacity(batteryLevel)
                .status(status)
                .build();
    }
}
