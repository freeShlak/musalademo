package atsibenko.musalademo.drone.controller;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.drone.dao.DroneDao;
import atsibenko.musalademo.drone.dto.DroneDto;
import atsibenko.musalademo.drone.util.DroneMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Drone specific endpoints.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/drone")
@CrossOrigin("${cors.origins}")
public class DroneController {
    private final DroneDao droneDao;

    /**
     * Get all available for delivery drones.
     *
     * @return list of idle drones with state info.
     */
    @ResponseBody
    @GetMapping("/available")
    public List<DroneDto> getAvailableDrones() {
        return droneDao.getAvailableDrones().stream()
                .map(DroneMapper::toDto)
                .toList();
    }

    /**
     * Get specific drone state.
     *
     * @param droneId id of drone.
     * @return drone state info.
     */
    @ResponseBody
    @GetMapping("/{droneId}/state")
    public DroneDto getState(@PathVariable UUID droneId) {
        return droneDao.findById(droneId)
                .map(DroneMapper::toDto)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND, "Unable to find drone with id " + droneId));
    }
}
