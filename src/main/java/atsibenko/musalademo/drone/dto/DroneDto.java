package atsibenko.musalademo.drone.dto;

import java.util.UUID;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneDto {
    private UUID id;
    private String serialNumber;
    private String model;
    private int weightLimit;
    private int batteryCapacity;
    private String status;
}
