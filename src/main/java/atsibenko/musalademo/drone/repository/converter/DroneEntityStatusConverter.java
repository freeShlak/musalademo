package atsibenko.musalademo.drone.repository.converter;

import atsibenko.musalademo.drone.repository.DroneEntityStatus;
import jakarta.persistence.AttributeConverter;

public class DroneEntityStatusConverter implements AttributeConverter<DroneEntityStatus, String> {
    @Override
    public String convertToDatabaseColumn(DroneEntityStatus attribute) {
        return attribute.toString();
    }

    @Override
    public DroneEntityStatus convertToEntityAttribute(String dbData) {
        return DroneEntityStatus.valueOf(dbData);
    }
}
