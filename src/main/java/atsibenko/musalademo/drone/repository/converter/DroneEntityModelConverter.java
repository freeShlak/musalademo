package atsibenko.musalademo.drone.repository.converter;

import atsibenko.musalademo.drone.repository.DroneEntityModel;
import jakarta.persistence.AttributeConverter;

public class DroneEntityModelConverter implements AttributeConverter<DroneEntityModel, String> {
    @Override
    public String convertToDatabaseColumn(DroneEntityModel attribute) {
        return attribute.toString();
    }

    @Override
    public DroneEntityModel convertToEntityAttribute(String dbData) {
        return DroneEntityModel.valueOf(dbData);
    }
}
