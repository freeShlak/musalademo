package atsibenko.musalademo.drone.repository;

public enum DroneEntityStatus {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING,
}
