package atsibenko.musalademo.drone.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DroneRepository extends JpaRepository<DroneEntity, UUID> {
    List<DroneEntity> findByStatusAndBatteryCapacityGreaterThanEqual(DroneEntityStatus status, int minCapacity);
}
