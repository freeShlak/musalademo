package atsibenko.musalademo.drone.repository;

import java.util.UUID;

import atsibenko.musalademo.drone.repository.converter.DroneEntityModelConverter;
import atsibenko.musalademo.drone.repository.converter.DroneEntityStatusConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "drone")
public class DroneEntity {

    @Id
    private UUID id;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "model")
    @Convert(converter = DroneEntityModelConverter.class)
    private DroneEntityModel model;

    @Column(name = "weight_limit")
    private int weightLimit;

    @Column(name = "battery_capacity")
    private int batteryCapacity;

    @Column(name = "status")
    @Convert(converter = DroneEntityStatusConverter.class)
    private DroneEntityStatus status;
}
