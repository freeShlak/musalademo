package atsibenko.musalademo.drone.repository;

public enum DroneEntityModel {
    LIGHTWEIGHT,
    MIDDLEWEIGHT,
    CRUISERWEIGHT,
    HEAVYWEIGHT,
}
