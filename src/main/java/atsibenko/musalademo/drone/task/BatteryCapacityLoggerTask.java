package atsibenko.musalademo.drone.task;

import java.time.Instant;

import atsibenko.musalademo.drone.dao.DroneDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@EnableScheduling
@Profile("!tests")
@RequiredArgsConstructor
public class BatteryCapacityLoggerTask {
    private final DroneDao droneDao;

    @Scheduled(cron = "0 * * * * ?")
    public void test() {
        log.info("Drones Battery level on {}", Instant.now());
        droneDao.getAllDrones().forEach(drone -> log.info("{} = {}%", drone.getId(), drone.getBatteryCapacity()));
    }
}
