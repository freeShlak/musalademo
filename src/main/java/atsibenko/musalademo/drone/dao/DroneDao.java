package atsibenko.musalademo.drone.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.drone.model.Drone;

public interface DroneDao {
    List<Drone> getAvailableDrones();

    Optional<Drone> findById(UUID id);

    Drone save(Drone drone);

    List<Drone> getAllDrones();
}
