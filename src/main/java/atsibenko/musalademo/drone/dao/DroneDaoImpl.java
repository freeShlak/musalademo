package atsibenko.musalademo.drone.dao;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.repository.DroneEntityStatus;
import atsibenko.musalademo.drone.repository.DroneRepository;
import atsibenko.musalademo.drone.util.DroneMapper;
import atsibenko.musalademo.drone.util.DroneValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DroneDaoImpl implements DroneDao {
    private final DroneRepository repository;

    @Override
    public List<Drone> getAvailableDrones() {
        return repository.findByStatusAndBatteryCapacityGreaterThanEqual(
                        DroneEntityStatus.IDLE,
                        DroneValidator.MINIMAL_BATTERY_CHARGE_PERCENT
                ).stream()
                .map(DroneMapper::map)
                .toList();
    }

    @Override
    public Optional<Drone> findById(UUID id) {
        return repository.findById(id).map(DroneMapper::map);
    }

    @Override
    public Drone save(Drone drone) {
        var entity = DroneMapper.map(drone);
        return DroneMapper.map(repository.save(entity));
    }

    @Override
    public List<Drone> getAllDrones() {
        return repository.findAll().stream().map(DroneMapper::map).toList();
    }
}
