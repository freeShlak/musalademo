package atsibenko.musalademo.drone.util;

import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.model.DroneStatus;
import lombok.experimental.UtilityClass;
import org.springframework.util.Assert;

@UtilityClass
public class DroneValidator {
    public final static int MINIMAL_BATTERY_CHARGE_PERCENT = 25;

    public static void validateAvailability(Drone drone) {
        Assert.isTrue(
                drone.getStatus() == DroneStatus.IDLE,
                String.format("Drone with id %s is unavailable for new delivery yet.", drone.getId())
        );
    }

    public static void validateMinBatteryLevel(Drone drone) {
        Assert.isTrue(
                drone.getBatteryCapacity() >= MINIMAL_BATTERY_CHARGE_PERCENT,
                String.format("Drone battery capacity is too low: %d%%.", drone.getBatteryCapacity())
        );
    }
}
