package atsibenko.musalademo.drone.util;

import atsibenko.musalademo.drone.dto.DroneDto;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.drone.model.DroneModel;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.drone.repository.DroneEntity;
import atsibenko.musalademo.drone.repository.DroneEntityModel;
import atsibenko.musalademo.drone.repository.DroneEntityStatus;
import lombok.experimental.UtilityClass;

@UtilityClass
public class DroneMapper {

    public static DroneDto toDto(Drone drone) {
        return DroneDto.builder()
                .id(drone.getId())
                .serialNumber(drone.getSerialNumber())
                .model(drone.getModel().getValue())
                .weightLimit(drone.getWeightLimit())
                .batteryCapacity(drone.getBatteryCapacity())
                .status(drone.getStatus().toString())
                .build();
    }

    public static Drone map(DroneEntity entity) {
        return Drone.builder()
                .id(entity.getId())
                .serialNumber(entity.getSerialNumber())
                .model(map(entity.getModel()))
                .weightLimit(entity.getWeightLimit())
                .batteryCapacity(entity.getBatteryCapacity())
                .status(map(entity.getStatus()))
                .build();
    }

    public static DroneEntity map(Drone entity) {
        return DroneEntity.builder()
                .id(entity.getId())
                .serialNumber(entity.getSerialNumber())
                .model(map(entity.getModel()))
                .weightLimit(entity.getWeightLimit())
                .batteryCapacity(entity.getBatteryCapacity())
                .status(map(entity.getStatus()))
                .build();
    }

    public static DroneModel map(DroneEntityModel model) {
        return switch (model) {
            case HEAVYWEIGHT -> DroneModel.HEAVYWEIGHT;
            case LIGHTWEIGHT -> DroneModel.LIGHTWEIGHT;
            case MIDDLEWEIGHT -> DroneModel.MIDDLEWEIGHT;
            case CRUISERWEIGHT -> DroneModel.CRUISERWEIGHT;
        };
    }

    public static DroneEntityModel map(DroneModel model) {
        return switch (model) {
            case HEAVYWEIGHT -> DroneEntityModel.HEAVYWEIGHT;
            case LIGHTWEIGHT -> DroneEntityModel.LIGHTWEIGHT;
            case MIDDLEWEIGHT -> DroneEntityModel.MIDDLEWEIGHT;
            case CRUISERWEIGHT -> DroneEntityModel.CRUISERWEIGHT;
        };
    }

    public static DroneStatus map(DroneEntityStatus status) {
        return switch (status) {
            case IDLE -> DroneStatus.IDLE;
            case LOADING -> DroneStatus.LOADING;
            case LOADED -> DroneStatus.LOADED;
            case DELIVERING -> DroneStatus.DELIVERING;
            case DELIVERED -> DroneStatus.DELIVERED;
            case RETURNING -> DroneStatus.RETURNING;
        };
    }

    public static DroneEntityStatus map(DroneStatus status) {
        return switch (status) {
            case IDLE -> DroneEntityStatus.IDLE;
            case LOADING -> DroneEntityStatus.LOADING;
            case LOADED -> DroneEntityStatus.LOADED;
            case DELIVERING -> DroneEntityStatus.DELIVERING;
            case DELIVERED -> DroneEntityStatus.DELIVERED;
            case RETURNING -> DroneEntityStatus.RETURNING;
        };
    }
}
