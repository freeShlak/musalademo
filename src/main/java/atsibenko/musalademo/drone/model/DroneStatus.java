package atsibenko.musalademo.drone.model;

public enum DroneStatus {
    IDLE,
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
    RETURNING,
}
