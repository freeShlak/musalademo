package atsibenko.musalademo.delivery.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.delivery.dao.DeliveryDao;
import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.delivery.util.DeliveryValidator;
import atsibenko.musalademo.drone.dao.DroneDao;
import atsibenko.musalademo.drone.model.DroneStatus;
import atsibenko.musalademo.drone.util.DroneValidator;
import atsibenko.musalademo.medication.dao.MedicationDao;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.util.MedicationUtil;
import atsibenko.musalademo.medication.util.MedicationValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class DeliveryService {
    private final DeliveryDao deliveryDao;
    private final MedicationDao medicationDao;
    private final DroneDao droneDao;

    @Transactional
    public Delivery createDelivery(UUID droneId, List<UUID> itemIds) {
        var drone = droneDao.findById(droneId).orElseThrow(() ->
                new IllegalArgumentException(String.format("Can't find drone with id %s.", droneId)));
        DroneValidator.validateAvailability(drone);
        DroneValidator.validateMinBatteryLevel(drone);

        var items = medicationDao.findByIds(itemIds);
        MedicationValidator.validateIdsConsistency(itemIds, items);
        MedicationValidator.validateAvailability(items);

        DeliveryValidator.validateOverweight(drone, items);

        drone = droneDao.save(drone.toBuilder().status(DroneStatus.LOADING).build());
        items = medicationDao.saveAll(items.stream()
                .map(item -> item.toBuilder().status(MedicationStatus.RESERVED).build())
                .toList());

        return deliveryDao.saveDelivery(
                Delivery.builder()
                        .id(UUID.randomUUID())
                        .items(items)
                        .drone(drone)
                        .capacity(MedicationUtil.getTotalWeight(items))
                        .status(DeliveryStatus.LOADING)
                        .build()
        );
    }

    @Transactional
    public Delivery registerDelivery(UUID deliveryId) {
        var delivery = deliveryDao.getFullInfo(deliveryId).orElseThrow(() ->
                new IllegalArgumentException(String.format("Can't find delivery with id %s.", deliveryId)));
        DeliveryValidator.validateStatusBeforeRegistration(delivery);

        var drone = delivery.getDrone();
        drone.setStatus(DroneStatus.DELIVERED);

        var items = Optional.ofNullable(delivery.getItems())
                .orElseThrow(() -> new IllegalStateException("Try to finish delivery without medication items."));
        items.forEach(item -> item.setStatus(MedicationStatus.UNAVAILABLE));

        delivery.setDrone(droneDao.save(drone));
        delivery.setItems(medicationDao.saveAll(items));
        delivery.setStatus(DeliveryStatus.DELIVERED);
        return deliveryDao.saveDelivery(delivery);
    }

    public Optional<Delivery> getShortInfo(UUID id) {
        return deliveryDao.getShortInfo(id);
    }

    public Optional<Delivery> getFullInfo(UUID id) {
        return deliveryDao.getFullInfo(id);
    }
}
