package atsibenko.musalademo.delivery.dto;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.medication.dto.MedicationDto;
import lombok.Builder;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@Builder
public class DeliveryDto {
    private UUID id;
    private UUID droneId;
    private int maxCapacity;
    private int capacity;
    private String status;

    @Nullable
    private List<MedicationDto> items;
}
