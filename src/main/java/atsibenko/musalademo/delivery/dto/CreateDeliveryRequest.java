package atsibenko.musalademo.delivery.dto;

import java.util.List;
import java.util.UUID;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateDeliveryRequest {
    @NotNull(message = "Specify drone ID")
    private UUID droneId;
    @NotEmpty(message = "Specify at least one medication item ID")
    private List<UUID> medicationItems;
}
