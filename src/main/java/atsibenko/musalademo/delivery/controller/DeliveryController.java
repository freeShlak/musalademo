package atsibenko.musalademo.delivery.controller;

import java.util.UUID;

import atsibenko.musalademo.delivery.dto.CreateDeliveryRequest;
import atsibenko.musalademo.delivery.dto.DeliveryDto;
import atsibenko.musalademo.delivery.service.DeliveryService;
import atsibenko.musalademo.delivery.util.DeliveryMapper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Delivery specific endpoints.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/delivery")
@CrossOrigin("${cors.origins}")
public class DeliveryController {
    private final DeliveryService service;

    /**
     * Create new delivery.
     *
     * @param request information about delivery.
     * @return full information about created delivery.
     */
    @PostMapping("/create")
    public DeliveryDto createDelivery(@Valid @RequestBody CreateDeliveryRequest request) {
        return DeliveryMapper.toDto(service.createDelivery(
                request.getDroneId(),
                request.getMedicationItems()
        ));
    }

    /**
     * Get short information about delivery. Only delivery and drone state included.
     *
     * @param deliveryId id of delivery, returned on delivery creation.
     * @return short information about created delivery.
     */
    @GetMapping("/{deliveryId}/short-info")
    public DeliveryDto getShortInfo(@PathVariable UUID deliveryId) {
        return service.getShortInfo(deliveryId)
                .map(DeliveryMapper::toDto)
                .orElseThrow(() -> new ResponseStatusException(
                        NOT_FOUND, "Unable to find delivery with id " + deliveryId));
    }

    /**
     * Get full information about delivery. Information about items in basket included.
     *
     * @param deliveryId id of delivery, returned on delivery creation.
     * @return full information about created delivery.
     */
    @GetMapping("/{deliveryId}/full-info")
    public DeliveryDto getFullInfo(@PathVariable UUID deliveryId) {
        return service.getFullInfo(deliveryId)
                .map(DeliveryMapper::toDto)
                .orElseThrow(() -> new ResponseStatusException(
                        NOT_FOUND, "Unable to find delivery with id " + deliveryId));
    }

    /**
     * Register successful delivery. Sends drone back.
     *
     * @param deliveryId id of delivery, returned on delivery creation.
     * @return lightweight state of delivery with confirmation of delivery ending.
     */
    @PostMapping("/{deliveryId}/register")
    public DeliveryDto registerDelivery(@PathVariable UUID deliveryId) {
        return DeliveryMapper.toDto(service.registerDelivery(deliveryId));
    }
}
