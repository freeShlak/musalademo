package atsibenko.musalademo.delivery.util;

import java.util.Collection;
import java.util.Optional;

import atsibenko.musalademo.delivery.dto.DeliveryDto;
import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.delivery.repository.DeliveryEntity;
import atsibenko.musalademo.delivery.repository.DeliveryEntityStatus;
import atsibenko.musalademo.drone.util.DroneMapper;
import atsibenko.musalademo.medication.util.MedicationMapper;
import lombok.experimental.UtilityClass;

@UtilityClass
public class DeliveryMapper {
    public DeliveryDto toDto(Delivery entity) {
        return DeliveryDto.builder()
                .id(entity.getId())
                .droneId(entity.getDrone().getId())
                .capacity(entity.getCapacity())
                .maxCapacity(entity.getDrone().getWeightLimit())
                .status(entity.getStatus().toString())
                .items(Optional.ofNullable(entity.getItems())
                        .map(items -> items.stream().map(MedicationMapper::toDto).toList())
                        .orElse(null))
                .build();
    }

    public Delivery mapShort(DeliveryEntity entity) {
        return Delivery.builder()
                .id(entity.getId())
                .drone(DroneMapper.map(entity.getDrone()))
                .capacity(entity.getCapacity())
                .items(null)
                .status(map(entity.getStatus()))
                .build();
    }

    public Delivery mapFull(DeliveryEntity entity) {
        return Delivery.builder()
                .id(entity.getId())
                .drone(DroneMapper.map(entity.getDrone()))
                .capacity(entity.getCapacity())
                .items(entity.getItems().stream().map(MedicationMapper::map).toList())
                .status(map(entity.getStatus()))
                .build();
    }

    public DeliveryEntity map(Delivery entity) {
        return DeliveryEntity.builder()
                .id(entity.getId())
                .drone(DroneMapper.map(entity.getDrone()))
                .capacity(entity.getCapacity())
                .items(Optional.ofNullable(entity.getItems()).stream()
                        .flatMap(Collection::stream)
                        .map(MedicationMapper::map).toList())
                .status(map(entity.getStatus()))
                .build();
    }

    public DeliveryStatus map(DeliveryEntityStatus status) {
        return switch (status) {
            case LOADING -> DeliveryStatus.LOADING;
            case LOADED -> DeliveryStatus.LOADED;
            case DELIVERING -> DeliveryStatus.DELIVERING;
            case DELIVERED -> DeliveryStatus.DELIVERED;
        };
    }

    public DeliveryEntityStatus map(DeliveryStatus status) {
        return switch (status) {
            case LOADING -> DeliveryEntityStatus.LOADING;
            case LOADED -> DeliveryEntityStatus.LOADED;
            case DELIVERING -> DeliveryEntityStatus.DELIVERING;
            case DELIVERED -> DeliveryEntityStatus.DELIVERED;
        };
    }
}
