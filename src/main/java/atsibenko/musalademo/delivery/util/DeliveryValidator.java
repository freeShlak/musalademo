package atsibenko.musalademo.delivery.util;

import java.util.List;

import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.model.DeliveryStatus;
import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.util.MedicationUtil;
import lombok.experimental.UtilityClass;
import org.springframework.util.Assert;

@UtilityClass
public class DeliveryValidator {

    public static void validateOverweight(Drone drone, List<Medication> items) {
        var maxWeight = drone.getWeightLimit();
        var summaryWeight = MedicationUtil.getTotalWeight(items);
        Assert.isTrue(
                summaryWeight <= maxWeight,
                String.format(
                        "Overweight. Drone capacity is %dgr, but items summary weight is %dgr.",
                        maxWeight, summaryWeight
                )
        );
    }

    public static void validateStatusBeforeRegistration(Delivery delivery) {
        var status = delivery.getStatus();
        Assert.isTrue(
                DeliveryStatus.DELIVERING.equals(status),
                String.format(
                        "Can't register delivery in status %s",
                        status
                )
        );
    }
}
