package atsibenko.musalademo.delivery.repository;

public enum DeliveryEntityStatus {
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
}
