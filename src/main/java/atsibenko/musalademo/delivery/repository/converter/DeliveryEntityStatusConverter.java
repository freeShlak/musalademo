package atsibenko.musalademo.delivery.repository.converter;

import atsibenko.musalademo.delivery.repository.DeliveryEntityStatus;
import jakarta.persistence.AttributeConverter;

public class DeliveryEntityStatusConverter implements AttributeConverter<DeliveryEntityStatus, String> {
    @Override
    public String convertToDatabaseColumn(DeliveryEntityStatus attribute) {
        return attribute.toString();
    }

    @Override
    public DeliveryEntityStatus convertToEntityAttribute(String dbData) {
        return DeliveryEntityStatus.valueOf(dbData);
    }
}
