package atsibenko.musalademo.delivery.repository;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.delivery.repository.converter.DeliveryEntityStatusConverter;
import atsibenko.musalademo.drone.repository.DroneEntity;
import atsibenko.musalademo.medication.repository.MedicationEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "delivery")
public class DeliveryEntity {
    @Id
    private UUID id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "drone_id")
    private DroneEntity drone;

    @Column(name = "capacity")
    private int capacity;

    @Column(name = "status")
    @Convert(converter = DeliveryEntityStatusConverter.class)
    private DeliveryEntityStatus status;

    @OneToMany(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinTable(
            name = "delivery_item",
            joinColumns = @JoinColumn(
                    name = "delivery_id",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "medication_id",
                    referencedColumnName = "id"))
    private List<MedicationEntity> items;
}
