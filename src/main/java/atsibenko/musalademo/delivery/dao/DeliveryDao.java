package atsibenko.musalademo.delivery.dao;

import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.delivery.model.Delivery;

public interface DeliveryDao {
    Optional<Delivery> getShortInfo(UUID id);

    Optional<Delivery> getFullInfo(UUID id);

    Delivery saveDelivery(Delivery delivery);
}
