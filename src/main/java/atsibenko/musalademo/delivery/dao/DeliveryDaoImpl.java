package atsibenko.musalademo.delivery.dao;

import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.delivery.model.Delivery;
import atsibenko.musalademo.delivery.repository.DeliveryRepository;
import atsibenko.musalademo.delivery.util.DeliveryMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class DeliveryDaoImpl implements DeliveryDao {
    private final DeliveryRepository repository;

    @Override
    public Optional<Delivery> getShortInfo(UUID id) {
        return repository.findById(id).map(DeliveryMapper::mapShort);
    }

    @Override
    public Optional<Delivery> getFullInfo(UUID id) {
        return repository.findById(id).map(DeliveryMapper::mapFull);
    }

    @Override
    public Delivery saveDelivery(Delivery delivery) {
        var entity = DeliveryMapper.map(delivery);
        return DeliveryMapper.mapFull(repository.save(entity));
    }
}
