package atsibenko.musalademo.delivery.model;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.drone.model.Drone;
import atsibenko.musalademo.medication.model.Medication;
import lombok.Builder;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@Builder(toBuilder = true)
public class Delivery {
    private UUID id;
    private int capacity;
    private DeliveryStatus status;
    private Drone drone;

    @Nullable
    private List<Medication> items;
}
