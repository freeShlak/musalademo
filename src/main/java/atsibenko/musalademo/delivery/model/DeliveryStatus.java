package atsibenko.musalademo.delivery.model;

public enum DeliveryStatus {
    LOADING,
    LOADED,
    DELIVERING,
    DELIVERED,
}
