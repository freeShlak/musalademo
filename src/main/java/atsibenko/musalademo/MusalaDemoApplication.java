package atsibenko.musalademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MusalaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MusalaDemoApplication.class, args);
    }

}
