package atsibenko.musalademo.configuration;

import atsibenko.musalademo.medication.util.MedicationUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * todo: rewrite image management in more appropriate way
 * todo: wrap everything into docker to prevent potential filesystem access
 * todo: add gradle sample images copying into default directory
 */
@Configuration
public class ResourcesConfiguration implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry
                .addResourceHandler(MedicationUtil.IMG_DEFAULT_ROOT_URL + "**")
                .addResourceLocations(
                        "classpath:/static/" + MedicationUtil.PLACEHOLDER_IMG,
                        "file:" + MedicationUtil.CUSTOM_IMAGE_ROOT_PATH
                );
    }
}
