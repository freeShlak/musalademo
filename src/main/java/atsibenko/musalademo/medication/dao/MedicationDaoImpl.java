package atsibenko.musalademo.medication.dao;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.repository.MedicationRepository;
import atsibenko.musalademo.medication.util.MedicationMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class MedicationDaoImpl implements MedicationDao {
    private final MedicationRepository repository;

    @Override
    public List<Medication> findByIds(Collection<UUID> ids) {
        return repository.findAllById(ids).stream()
                .map(MedicationMapper::map)
                .toList();
    }

    @Override
    public List<Medication> saveAll(Collection<Medication> items) {
        var entities = items.stream().map(MedicationMapper::map).toList();
        return repository.saveAll(entities).stream()
                .map(MedicationMapper::map)
                .toList();
    }

    @Override
    public Page<Medication> findAllByStatus(Collection<MedicationStatus> statuses,
                                            Pageable pageable) {
        var found = repository.findAllByStatusIn(statuses.stream().map(MedicationMapper::map).toList(), pageable);
        return new PageImpl<>(
                found.getContent().stream().map(MedicationMapper::map).toList(),
                found.getPageable(),
                found.getTotalElements()
        );
    }

    @Override
    public Page<Medication> findAllByIdsAndStatus(Collection<UUID> ids,
                                                  Collection<MedicationStatus> statuses,
                                                  Pageable pageable) {
        var found = repository.findAllByIdInAndStatusIn(
                ids, statuses.stream().map(MedicationMapper::map).toList(), pageable
        );

        return new PageImpl<>(
                found.getContent().stream().map(MedicationMapper::map).toList(),
                found.getPageable(),
                found.getTotalElements()
        );
    }
}
