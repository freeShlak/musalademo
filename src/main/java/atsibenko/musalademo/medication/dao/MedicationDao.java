package atsibenko.musalademo.medication.dao;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MedicationDao {
    List<Medication> findByIds(Collection<UUID> id);

    List<Medication> saveAll(Collection<Medication> items);

    Page<Medication> findAllByStatus(Collection<MedicationStatus> statuses,
                                     Pageable pageable);

    Page<Medication> findAllByIdsAndStatus(Collection<UUID> ids,
                                           Collection<MedicationStatus> statuses,
                                           Pageable pageable);
}
