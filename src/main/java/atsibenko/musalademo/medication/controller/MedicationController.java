package atsibenko.musalademo.medication.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import atsibenko.musalademo.medication.dao.MedicationDao;
import atsibenko.musalademo.medication.dto.MedicationDto;
import atsibenko.musalademo.medication.dto.MedicationStatusDto;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.util.MedicationMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Medication specific endpoints.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/medication")
@CrossOrigin("${cors.origins}")
public class MedicationController {
    private final MedicationDao medicationDao;

    /**
     * Get medications according to requested ids, status and pagination params.
     *
     * @param ids      Optional filter for medication ids. Ignores this filter if not specified.
     * @param status   Optional filter for medication status. Ignores this filter if not specified.
     * @param pageable Pagination parameters for request.
     * @return Page of available medications.
     */
    @GetMapping
    @ResponseBody
    public Page<MedicationDto> page(
            @RequestParam(required = false) @Nullable List<UUID> ids,
            @RequestParam(required = false) @Nullable MedicationStatusDto status,
            @PageableDefault Pageable pageable
    ) {
        var searchingStatuses = Optional.ofNullable(status)
                .map(MedicationMapper::map)
                .orElse(Arrays.asList(MedicationStatus.values()));

        var medications = Optional.ofNullable(ids)
                .map(searchingIds -> medicationDao.findAllByIdsAndStatus(searchingIds, searchingStatuses, pageable))
                .orElseGet(() -> medicationDao.findAllByStatus(searchingStatuses, pageable));

        return new PageImpl<>(
                medications.getContent().stream()
                        .map(medication -> MedicationMapper.toDto(medication)
                                .toBuilder()
                                .status(MedicationMapper.toDto(medication.getStatus()))
                                .build())
                        .toList(),
                medications.getPageable(),
                medications.getTotalElements()
        );
    }
}
