package atsibenko.musalademo.medication.util;

import java.util.List;

import atsibenko.musalademo.medication.dto.MedicationDto;
import atsibenko.musalademo.medication.dto.MedicationStatusDto;
import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import atsibenko.musalademo.medication.repository.MedicationEntity;
import atsibenko.musalademo.medication.repository.MedicationEntityStatus;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MedicationMapper {

    public MedicationDto toDto(Medication entity) {
        return MedicationDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .code(entity.getCode())
                .weight(entity.getWeight())
                .imgUrl(MedicationUtil.resolveImagePath(entity))
                .build();
    }

    public Medication map(MedicationEntity entity) {
        return Medication.builder()
                .id(entity.getId())
                .name(entity.getName())
                .code(entity.getCode())
                .weight(entity.getWeight())
                .status(map(entity.getStatus()))
                .imgUrl(entity.getImgUrl())
                .build();
    }

    public MedicationEntity map(Medication entity) {
        return MedicationEntity.builder()
                .id(entity.getId())
                .name(entity.getName())
                .code(entity.getCode())
                .weight(entity.getWeight())
                .status(map(entity.getStatus()))
                .imgUrl(entity.getImgUrl())
                .build();
    }

    public MedicationStatus map(MedicationEntityStatus status) {
        return switch (status) {
            case AVAILABLE -> MedicationStatus.AVAILABLE;
            case RESERVED -> MedicationStatus.RESERVED;
            case UNAVAILABLE -> MedicationStatus.UNAVAILABLE;
        };
    }

    public MedicationEntityStatus map(MedicationStatus status) {
        return switch (status) {
            case AVAILABLE -> MedicationEntityStatus.AVAILABLE;
            case RESERVED -> MedicationEntityStatus.RESERVED;
            case UNAVAILABLE -> MedicationEntityStatus.UNAVAILABLE;
        };
    }

    public List<MedicationStatus> map(MedicationStatusDto status) {
        return switch (status) {
            case AVAILABLE -> List.of(MedicationStatus.AVAILABLE);
            case UNAVAILABLE -> List.of(MedicationStatus.UNAVAILABLE, MedicationStatus.RESERVED);
        };
    }

    public MedicationStatusDto toDto(MedicationStatus status) {
        return switch (status) {
            case AVAILABLE -> MedicationStatusDto.AVAILABLE;
            case RESERVED, UNAVAILABLE -> MedicationStatusDto.UNAVAILABLE;
        };
    }
}
