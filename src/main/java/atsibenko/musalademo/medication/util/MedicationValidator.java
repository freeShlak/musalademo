package atsibenko.musalademo.medication.util;

import java.util.List;
import java.util.UUID;

import atsibenko.musalademo.medication.model.Medication;
import atsibenko.musalademo.medication.model.MedicationStatus;
import lombok.experimental.UtilityClass;
import org.springframework.util.Assert;

@UtilityClass
public class MedicationValidator {

    public static void validateIdsConsistency(List<UUID> itemIds, List<Medication> items) {
        var foundIds = items.stream().map(Medication::getId).toList();
        itemIds.forEach(id -> Assert.isTrue(
                foundIds.contains(id),
                String.format("Can't find medication item with id %s.", id.toString())
        ));
    }

    public static void validateAvailability(List<Medication> items) {
        items.forEach(item -> Assert.isTrue(
                item.getStatus() == MedicationStatus.AVAILABLE,
                String.format("Medication item with id %s is unavailable.", item.getId().toString())
        ));
    }

}
