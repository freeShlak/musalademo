package atsibenko.musalademo.medication.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

import atsibenko.musalademo.medication.model.Medication;
import lombok.experimental.UtilityClass;

@UtilityClass
public class MedicationUtil {
    public final static String IMG_DEFAULT_ROOT_URL = "/medication/image/";
    public final static String CUSTOM_IMAGE_ROOT_PATH = "./data/medication/image/";
    public final static String PLACEHOLDER_IMG = "placeholder.jpg";

    public int getTotalWeight(List<Medication> items) {
        return items.stream().mapToInt(Medication::getWeight).sum();
    }

    public String resolveImagePath(Medication medication) {
        return IMG_DEFAULT_ROOT_URL + Optional.ofNullable(medication.getImgUrl())
                .filter(url -> !url.isBlank())
                .filter(url -> Files.isReadable(Path.of(CUSTOM_IMAGE_ROOT_PATH + url)))
                .orElse(PLACEHOLDER_IMG);
    }
}
