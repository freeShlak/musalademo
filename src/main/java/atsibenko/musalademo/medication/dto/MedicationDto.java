package atsibenko.musalademo.medication.dto;

import java.util.UUID;

import lombok.Builder;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@Builder(toBuilder = true)
public class MedicationDto {
    private UUID id;
    private String name;
    private String code;
    private int weight;
    private String imgUrl;
    @Nullable
    private MedicationStatusDto status;
}
