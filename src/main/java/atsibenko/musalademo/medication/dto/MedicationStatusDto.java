package atsibenko.musalademo.medication.dto;

public enum MedicationStatusDto {
    AVAILABLE,
    UNAVAILABLE,
}
