package atsibenko.musalademo.medication.repository;

import java.util.UUID;

import atsibenko.musalademo.medication.repository.converter.MedicationEntityStatusConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;

@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Table(name = "medication")
public class MedicationEntity {

    @Id
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "weight")
    private int weight;

    @Column(name = "code")
    private String code;

    @Column(name = "status")
    @Convert(converter = MedicationEntityStatusConverter.class)
    private MedicationEntityStatus status;

    @Nullable
    @Column(name = "image_url")
    private String imgUrl;
}
