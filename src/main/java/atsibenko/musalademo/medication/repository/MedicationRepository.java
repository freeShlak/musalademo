package atsibenko.musalademo.medication.repository;

import java.util.Collection;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<MedicationEntity, UUID> {
    Page<MedicationEntity> findAllByStatusIn(Collection<MedicationEntityStatus> status,
                                             Pageable pageable);

    Page<MedicationEntity> findAllByIdInAndStatusIn(Collection<UUID> id,
                                                    Collection<MedicationEntityStatus> status,
                                                    Pageable pageable);
}
