package atsibenko.musalademo.medication.repository.converter;

import atsibenko.musalademo.medication.repository.MedicationEntityStatus;
import jakarta.persistence.AttributeConverter;

public class MedicationEntityStatusConverter implements AttributeConverter<MedicationEntityStatus, String> {
    @Override
    public String convertToDatabaseColumn(MedicationEntityStatus attribute) {
        return attribute.toString();
    }

    @Override
    public MedicationEntityStatus convertToEntityAttribute(String dbData) {
        return MedicationEntityStatus.valueOf(dbData);
    }
}
