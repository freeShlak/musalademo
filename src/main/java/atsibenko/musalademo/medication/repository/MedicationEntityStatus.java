package atsibenko.musalademo.medication.repository;

public enum MedicationEntityStatus {
    AVAILABLE,
    RESERVED,
    UNAVAILABLE,
}
