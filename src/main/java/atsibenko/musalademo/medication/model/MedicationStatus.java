package atsibenko.musalademo.medication.model;

public enum MedicationStatus {
    AVAILABLE,
    RESERVED,
    UNAVAILABLE,
}
