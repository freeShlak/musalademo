package atsibenko.musalademo.medication.model;

import java.util.UUID;

import lombok.Builder;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@Builder(toBuilder = true)
public class Medication {
    private UUID id;
    private String name;
    private String code;
    private int weight;
    private MedicationStatus status;
    @Nullable
    private String imgUrl;
}
