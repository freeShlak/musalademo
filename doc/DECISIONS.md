#### Registering a drone
Assignment says:
1) > Develop a service via REST API that allows clients to communicate with the drones.
2) > We have a fleet of **10 drones**.
3) > The specific communicaiton with the drone is outside the scope of this task.

So drone registration doesn't seem like part of this application.
Because of this reason I've decided that 'registering drone' requirement means something like that:
> User can register successful order delivery after receiving needed medication items.

#### Check drone battery level for a given drone
Use `drone/{droneId}/state` to extract this info.
Information about only battery level is not common case for end-user communication with drone.
Probably user don't want to poll this handle everytime just for check battery level.
In my opinion user is more interested in delivery status and other order summary.

#### Medication
Seems like each medication item has it's own data.
For example, all items have different photos and weight even if it's one type of medicine with the same name.

#### Same DTO for different handles
I've decided to use same dto objects for different handles just to avoid a lot of duplicated information.
If I don't need some fields in specific handle, I filter out it from response with object mapper configuration and null field value.
It's always possible to add new DTO for a specific case.
